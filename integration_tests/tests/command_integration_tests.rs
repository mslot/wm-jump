use jump_lib::command;

#[test]
fn call_wmctrl() {
    let output = command::wmctrl_call().unwrap();
    assert_eq!("", output)
}
