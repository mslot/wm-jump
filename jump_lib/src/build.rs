use protoc_rust::Customize;

fn main() {
    protoc_rust::run(protoc_rust::Args {
        out_dir: "src/",
        input: &["src/messages.proto"],
        includes: &["src/"],
        customize: Customize {
            ..Default::default()
        },
    })
    .expect("protoc");
}
