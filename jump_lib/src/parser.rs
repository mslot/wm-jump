#![crate_name = "jump_lib"]

use crate::Parser;
use std::io::{self, BufRead, BufReader, Cursor};

/// A struct representing the line of the `wmctrl` command line output when run with `-lG` parameters
pub struct WmCtrlLine {
    pub title: String,
    pub window_id: String,
    pub desktop_id: i32,
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
}

///A WmCtrl parser
///
///This parser can parse lines outputted from `wmctrl` commandline tool when `wmctrl` is run with `-lG` parameters: `wmctrl -lG`
///
/// # Arguments
///
/// `lines` - a multiline string representing all lines from the `wmctrl` output with newlines
///
/// # Outputs
///
/// A vector with all lines. Each line is represented by a `WmCtrlLine`
///
/// # Examples
///
/// ```
///
/// use jump_lib::{parser::WmCtrlParser, Parser};
/// let parser = WmCtrlParser {};
///
/// //window id, desktop id, x, y, width, height, title
/// let nodes = parser.parse(
/// "0x004000aa  0 19   99   1522 1328 styx emacs@styx
/// 0x004000aa2  2 12   92   1522 1322 styx emacs@styx2",
/// );
///
/// assert_eq!(2, nodes.len());
///```
pub struct WmCtrlParser {}

impl Parser<&str, WmCtrlLine> for WmCtrlParser {
    /// this function parses all lines
    ///
    /// It basically calls `parse_line` on each line after splitting the string up on newlines, `\n`
    fn parse(&self, lines: &str) -> Vec<WmCtrlLine> {
        let mut nodes: Vec<WmCtrlLine> = Vec::new();

        String::from(lines).lines().for_each(|l| {
            let parsed_info = self.parse_line(l);
            nodes.push(parsed_info);
        });

        nodes
    }
}

impl WmCtrlParser {
    ///this function splits each line up. This function is not relying on `split_whitespace`, but uses a home made algorithm, that is ... properly slower than rusts `split_whitespace`, but I had FUN writing it. This might change in the future.
    ///
    /// # Arguments
    /// `line` - a line from the output of wmctrl when run with the `-lG` parameters.
    ///
    /// # Example
    /// A line is looking like this when coming from `wmctrl -lG`:
    ///
    /// * `0x004000aa  0 19   99   1522 1328 styx emacs@styx"`
    ///
    /// when splitting it up and removing all whitespaces (except for the last column), the columns is named like this
    ///
    /// * window id, desktop id, x, y, width, height, title
    ///
    /// the last column, `title` can have multiple whitespaces, that shouldn't be split. When my algorithm sees the last column, it takes all chars to the end of the line and `break`.
    pub fn parse_line(&self, l: &str) -> WmCtrlLine {
        let mut title: String = String::new();
        let mut window_id: String = String::new();
        let mut desktop_id: i32 = -1;
        let mut x: i32 = -1;
        let mut y: i32 = -1;
        let mut width: i32 = -1;
        let mut height: i32 = -1;

        let mut section = 0;
        let mut start = 0;
        let mut end = 0;
        let mut word_detected = false;

        for (i, c) in l.chars().enumerate() {
            if !c.is_whitespace() && !word_detected {
                word_detected = true;
                start = i;
            }

            if c.is_whitespace() && word_detected {
                word_detected = false;
                end = i;

                match section {
                    0 => {
                        window_id = l[start..end].into();
                    }
                    1 => {
                        desktop_id = l[start..end].parse::<i32>().unwrap();
                    }
                    2 => {
                        x = l[start..end].parse::<i32>().unwrap();
                    }
                    3 => {
                        y = l[start..end].parse::<i32>().unwrap();
                    }
                    4 => {
                        width = l[start..end].parse::<i32>().unwrap();
                    }
                    5 => {
                        height = l[start..end].parse::<i32>().unwrap();
                    }
                    6 => {
                        title = l[start..].into();
                        break;
                    }
                    _ => {}
                }
                section = section + 1;
            }
        }

        WmCtrlLine {
            title,
            window_id,
            desktop_id,
            x,
            y,
            width,
            height,
        }
    }
}
