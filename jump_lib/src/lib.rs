pub mod messages;
pub mod command;
pub mod core;
pub mod parser;

pub trait Parser<I, O> {
    fn parse(&self, obj: I) -> Vec<O>;
}

#[cfg(test)]
mod tests {
    use crate::{parser::WmCtrlParser, Parser};

    #[test]
    fn parse_empty_string() {
        let parser = WmCtrlParser {};
        let nodes = parser.parse("");

        assert_eq!(0, nodes.len());
    }

    #[test]
    fn parse_single_line() {
        let parser = WmCtrlParser {};

        //window id, desktop id, x, y, width, height, title
        let nodes = parser.parse("0x004000aa  0 19   99   1522 1328 styx emacs@styx");

        assert_eq!(1, nodes.len());

        let first_node = nodes.first().unwrap();

        assert_eq!(19, first_node.x);
        assert_eq!(99, first_node.y);
        assert_eq!(1522, first_node.width);
        assert_eq!(1328, first_node.height);
        assert_eq!(0, first_node.desktop_id);
        assert_eq!("styx emacs@styx", first_node.title);
        assert_eq!("0x004000aa", first_node.window_id);
    }

    #[test]
    fn parse_two_lines() {
        let parser = WmCtrlParser {};

        //window id, desktop id, x, y, width, height, title
        let nodes = parser.parse(
            "0x004000aa  0 19   99   1522 1328 styx emacs@styx
 0x004000aa2  2 12   92   1522 1322 styx emacs@styx2",
        );

        assert_eq!(2, nodes.len());

        let first_node = nodes.first().unwrap();

        assert_eq!(19, first_node.x);
        assert_eq!(99, first_node.y);
        assert_eq!(1522, first_node.width);
        assert_eq!(1328, first_node.height);
        assert_eq!(0, first_node.desktop_id);
        assert_eq!("styx emacs@styx", first_node.title);
        assert_eq!("0x004000aa", first_node.window_id);

        let second_node = nodes.get(1).unwrap();

        assert_eq!(12, second_node.x);
        assert_eq!(92, second_node.y);
        assert_eq!(1522, second_node.width);
        assert_eq!(1322, second_node.height);
        assert_eq!(2, second_node.desktop_id);
        assert_eq!("styx emacs@styx2", second_node.title);
        assert_eq!("0x004000aa2", second_node.window_id);
    }
}
