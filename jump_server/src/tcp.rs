use std::{
    sync::{atomic::AtomicBool, atomic::Ordering, Arc},
    thread,
};

use Ordering::Relaxed;

use tokio::{io::{AsyncReadExt, AsyncWriteExt}, net::{TcpListener, TcpStream}, task};

pub struct JumpServer {
    stop: AtomicBool,
    address: String,
}

pub struct JumpServerHost {
    server: Arc<JumpServer>,
    address: String,
}

impl JumpServerHost {
    pub fn new(address: &str) -> JumpServerHost {
        JumpServerHost {
            server: Arc::new(JumpServer::new(address)),
            address: String::from(address),
        }
    }

    pub async fn start_and_bind(&self) {
        println!("starting server ...");
        self.server.start_and_bind().await;
    }

    pub async fn start(&self, listener: TcpListener) {
        println!("starting server ...");
        self.server.start(listener).await;
    }

    pub async fn stop(&self) {
        self.server.stop();

        let close_stream = TcpStream::connect(&self.address).await;
        let mut close_stream = match close_stream {
            Ok(v) => v,
            Err(e) => {
                // The server might already be closed - do nothing
                return;
            }
        };

        close_stream.write(b"").await;
        close_stream.flush().await;
        close_stream.shutdown().await;

        println!("stopped");
    }
}

impl JumpServer {
    pub fn new(address: &str) -> JumpServer {
        JumpServer {
            address: String::from(address),
            stop: AtomicBool::new(false),
        }
    }

    pub async fn start(&self, listener: TcpListener) {
        if !self.stop.load(Relaxed) {
            loop {
                println!("starting to listen ... ");
                let (mut socket, _) = listener.accept().await.unwrap();
                println!("got connection");

                //let message: HelloResponse;
                //message = HelloResponse {
                    //message: String::from("hello"),
                //};

                let mut buf: Vec<u8> = Vec::with_capacity(100);

                //message.encode(&mut buf).unwrap();

                socket.write(&buf).await.unwrap();
                socket.flush().await.unwrap();

                if self.stop.load(Relaxed) {
                    println!("stopping ...");
                    break;
                }
            }
        } else {
            println!("server already stopped ... stopping");
        }
    }

    pub async fn start_and_bind(&self) {
        println!("binding to address");
        let listener = TcpListener::bind(self.address.as_str()).await.unwrap();
        println!("bound to address");

        self.start(listener).await;
    }

    pub fn stop(&self) {
        self.stop.store(true, Relaxed);
    }
}
